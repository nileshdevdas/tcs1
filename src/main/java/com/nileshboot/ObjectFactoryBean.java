package com.nileshboot;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class ObjectFactoryBean {
	public static void main(String[] args) throws Exception {
		ObjectFactoryBean factoryBean = new ObjectFactoryBean();
		factoryBean.getObject("employee");
		factoryBean.publish("xxxxxxxxxxxxx");
		startEmbeddedContainer();
	}

	private static Server server = null;

	public static void startEmbeddedContainer() throws Exception {
		Server server = new Server(8080);
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);
		context.addServlet(new ServletHolder(new HelloWorld()), "/hello");
		server.start();
	}

	static class HelloWorld extends HttpServlet {
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			ObjectMapper mapper = new ObjectMapper();
			Payload payload = new Payload();
			payload.setId(1L);
			payload.setName("Nilesh");
			String responseJSON = mapper.writeValueAsString(payload);
			resp.setContentType("application/json");
			PrintWriter write = resp.getWriter();
			write.write(responseJSON);
			write.flush();
		}
	}

	private static final Map<String, Object> instances = new HashMap<String, Object>();
	private static final Properties properties = new Properties();
	static {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				instances.values().iterator().forEachRemaining((instance) -> {
					if (instance instanceof ILifecycle) {
						ILifecycle l = (ILifecycle) instance;
						l.destroy();
					}
				});
			}
		}));
	}

	public Object getObject(String name) {
		Object theObject = null;
		if (instances.containsKey(name)) {
			return instances.get(name);
		} else {
			try {
				theObject = Class.forName("nileshboot.EmployeeBean").newInstance();
				if (theObject instanceof ILifecycle) {
					ILifecycle lf = (ILifecycle) theObject;
					lf.init();
					instances.put("employee", theObject);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return theObject;
	}

	public void publish(Object payload) {
		ObjectFactoryBean.instances.values().stream().forEach((instance) -> {
			if (instance instanceof ISubscriptionAware) {
				ISubscriptionAware a = (ISubscriptionAware) instance;
				a.subscribe(payload);
			}
		});
	}
}

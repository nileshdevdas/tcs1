package com.nileshboot;

public interface ISubscriptionAware {
	public void subscribe(Object payload);
}

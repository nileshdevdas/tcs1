package com.nileshboot;

public class EmployeeBean implements ILifecycle, ISubscriptionAware {
	@Override
	public void destroy() {
		System.out.println("destroy");
	}

	@Override
	public void init() {
		System.out.println("Init");

	}

	@Override
	public void subscribe(Object payload) {
		System.out.println("Just got some payload....");
	}
}

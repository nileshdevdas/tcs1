package com.nileshboot;

public interface ILifecycle {

	void init();

	void destroy();
}

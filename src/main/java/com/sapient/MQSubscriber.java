package com.sapient;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MQSubscriber {

	public static void main(String[] args) {
		Connection connection = null;
		try {
			connection = new ActiveMQConnectionFactory("tcp://localhost:61616").createConnection("admin", "admin");
			connection.start();
			Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
			Topic queue = session.createTopic("LANDINGS");
			MessageConsumer consumer = session.createConsumer(queue);
			System.out.println("Waiting for Event BroadCast .....");
			while (true) {
				Message message = consumer.receive();
				System.out.println(message);
			}

		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}

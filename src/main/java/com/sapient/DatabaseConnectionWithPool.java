package com.sapient;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DatabaseConnectionWithPool {

	private static HikariDataSource dataSource = null;
	private static final HikariConfig config = new HikariConfig();

	static {
		config.setDriverClassName("com.mysql.cj.jdbc.Driver");
		config.setJdbcUrl("jdbc:mysql://localhost:3306/test");
		config.setUsername("root");
		config.setMaximumPoolSize(100);
		config.setMinimumIdle(50);
		config.setConnectionTimeout(5000);
		dataSource = new HikariDataSource(config);
	}

	
	public static void main(String[] args) throws Exception {
		while (true)
			System.out.println(dataSource.getConnection());
	}

}

package com.sapient;

import javax.jms.Connection;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MQRecvr {

	public static void main(String[] args) throws Exception {
		Connection connection = null;
		connection = new ActiveMQConnectionFactory("tcp://localhost:61616").createConnection("admin", "admin");
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue("SAPIENT");
		MessageConsumer consumer = session.createConsumer(queue);
		System.out.println("Waiting........for message");
		while (true) {
			Message message = consumer.receive();
			if (message instanceof ObjectMessage) {
				ObjectMessage om = (ObjectMessage) message;
				UserInfo user = (UserInfo) om.getObject();
				System.out.println(user.getName());
			}
		}
//		session.close();
//		connection.close();

	}

}

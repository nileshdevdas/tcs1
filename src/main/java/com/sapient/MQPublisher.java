package com.sapient;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MQPublisher {

	public static void main(String[] args) {
		Connection connection = null;
		try {

			connection = new ActiveMQConnectionFactory("tcp://localhost:61616").createConnection("admin", "admin");
			Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
			Topic topic= session.createTopic("LANDINGS");
			TextMessage message = session.createTextMessage();
			message.setText("This is a sample Message");
			MessageProducer sender = session.createProducer(topic);
			sender.send(message);
			session.commit();
			session.close();
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}

package com.sapient;

import java.util.Properties;
import java.util.UUID;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaConsumerDemo {

	public static void main(String[] args) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.10.100:9092,192.168.10.101:9092");
		props.put("acks", "all");
		// if failed should i retry
		props.put("retries", 3);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		String[] users = new String[] { "nilesh", "user2", "user3", "user4", "user5" };
		Producer<String, String> producer = new KafkaProducer(props);
		for (int j = 0; j < users.length; j++) {
			producer.send(new ProducerRecord<String, String>("SONGS", users[j], UUID.randomUUID().toString()));
		}
		producer.close();
	}

}

package com.sapient;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Cluster.Builder;
import com.datastax.driver.core.Session;

public class CassandraBaseDemo {

	public static void main(String[] args) {
		Session session = null;
		Cluster cluster = null;
		try {
			Builder builder = Cluster.builder().addContactPoint("192.168.5.100").addContactPoint("192.168.5.101");
			builder.withPort(9042);
			cluster = builder.build();
			session = cluster.connect();
			for (int i = 0; i < 1000000; i++) {
				session.execute("select * from sapient.employees");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
			if (cluster != null)
				cluster.close();
		}
	}

}

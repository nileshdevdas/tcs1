package com.sapient;

import java.util.UUID;

import javax.jms.Connection;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MQSender {
	public static void main(String[] args) throws Exception {
		Connection connection = null;
		connection = new ActiveMQConnectionFactory("tcp://localhost:61616").createConnection("admin", "admin");
		Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue("SAPIENT");
		ObjectMessage message = session.createObjectMessage();
		UserInfo info = new UserInfo();
		info.setAction("xxx");
		info.setId(1L);
		info.setName("nilesh");
		info.setStatus("Success");
		message.setObject(info);
		MessageProducer sender = session.createProducer(queue);
		sender.send(message);
		session.commit();
		session.close();
		connection.close();
	}
}

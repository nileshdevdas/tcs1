package com.sapient;

import java.sql.Connection;
import java.sql.Statement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DatabaseTransactionDemo {
	private static HikariDataSource dataSource = null;
	private static final HikariConfig config = new HikariConfig();

	static {
		config.setDriverClassName("com.mysql.cj.jdbc.Driver");
		config.setJdbcUrl("jdbc:mysql://localhost:3306/test");
		config.setUsername("root");
		config.setMaximumPoolSize(100);
		config.setMinimumIdle(50);
		config.setConnectionTimeout(5000);
		dataSource = new HikariDataSource(config);
	}

	public static void main(String[] args) throws Exception {
		Connection con = null;
		try {
			con = dataSource.getConnection();
			con.setAutoCommit(false);
			Statement st = con.createStatement();
			st.executeUpdate("insert into tmtest values(1)");
			int b = 1 / 12;
			con.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (con != null)
				con.close();
		}
	}

}

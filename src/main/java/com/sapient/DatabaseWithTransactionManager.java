package com.sapient;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.XAConnection;
import javax.jms.XAConnectionFactory;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;
import javax.transaction.xa.XAResource;

import org.apache.activemq.ActiveMQXAConnectionFactory;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.atomikos.jms.AtomikosConnectionFactoryBean;

import bitronix.tm.TransactionManagerServices;

public class DatabaseWithTransactionManager {

	public static void a(String payload, UserTransaction tm) throws Exception {
		System.out.println("Doing a activity");
		tm.setRollbackOnly();
	}

	public static void b(String payload, UserTransaction tm) throws Exception {
		tm.setRollbackOnly();
	}

	public static void c(String payload, UserTransaction tm) throws Exception {
		System.out.println("Doing c activity");
		tm.setRollbackOnly();
	}

	public static void d(String payload, UserTransaction tm) throws Exception {
		System.out.println("Doing d activity");
		tm.setRollbackOnly();
	}

	public static void main(String[] args) {
		/** Transaction Manager Creation */
		UserTransaction tm = new UserTransactionImp();
		try {
			tm.setTransactionTimeout(5000);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		try {
			tm.begin();
			a("", tm);
			b("", tm);
			c("", tm);
			d("", tm);
			tm.commit();
		} catch (Exception e2) {
			e2.printStackTrace();
		}

		// this is where the transaction manager is initialized
		/**************************** DATABASE RESOURCE ***********************/
		Properties props2 = new Properties();
		props2.put("url", "jdbc:mysql://localhost:3306/test1");
		props2.put("user", "root");
		props2.put("password", "");
		AtomikosDataSourceBean xaDataSourceBean2 = new AtomikosDataSourceBean();
		xaDataSourceBean2.setUniqueResourceName("sp2");
		xaDataSourceBean2.setXaDataSourceClassName("com.mysql.cj.jdbc.MysqlXADataSource");
		xaDataSourceBean2.setXaProperties(props2);
		/**************************** DATABASE RESOURCE ***********************/

		/***************************************************/
		XAConnectionFactory xacf = new ActiveMQXAConnectionFactory();
		AtomikosConnectionFactoryBean cf = new AtomikosConnectionFactoryBean();
		cf.setUniqueResourceName("activemq");
		cf.setXaConnectionFactory(xacf);
		cf.setPoolSize(5);
		/***************************************************/
		/***********************************
		 * DATABASE ACTIVITY
		 ***********************************/
		Connection con2 = null;
		/**************************************
		 * JMS ACTIVITY
		 *************************************/
		javax.jms.Connection con = null;
		Session session = null;
		try {
			tm.begin();
			con2 = xaDataSourceBean2.getConnection();
			Statement statement2 = con2.createStatement();
			statement2.executeUpdate("insert into tmtest values('1')");
			/***********************************
			 * DATABASE ACTIVITY
			 ***********************************/
			con = cf.createConnection();
			session = con.createSession(true, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue("SAPIENT");
			ObjectMessage message = session.createObjectMessage();
			UserInfo info = new UserInfo();
			tm.setRollbackOnly();
			info.setAction("xxx");
			info.setId(1L);
			info.setName("nilesh");
			info.setStatus("Success");
			message.setObject(info);
			MessageProducer sender = session.createProducer(queue);
			sender.send(message);
			tm.commit();
			/**************************************
			 * JMS ACTIVITY
			 *************************************/
		} catch (Exception e) {
			e.printStackTrace();
			try {
				// i am okay you decide to rollback not
				// i marked this set of transactionn as roolbacked for
				tm.setRollbackOnly();
				// tm.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		try {
			if (con2 != null)
				con2.close();
			if (session != null)
				session.close();
			if (con != null)
				con.close();
			if (cf != null)
				cf.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}

package com.sapient;

import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaProducerDemo {
	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.10.100:9092,192.168.10.101:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		String[] runways = new String[] { "runway1", "runway2", "runway3", "runway4", "runway5" };
		Producer<String, String> producer = new KafkaProducer(props);
		for (int i = 0; i < 200000; i++) {
			for (int j = 0; j < runways.length; j++) {
				System.out.println("Landing a Flight on " + runways[j]);
				producer.send(new ProducerRecord<String, String>("LANDINGS", runways[j],
						"Flight-" + UUID.randomUUID().toString()));
			}
		}
		producer.close();
	}
}

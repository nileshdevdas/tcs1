package com.sapient;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Properties;

import javax.transaction.UserTransaction;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.jdbc.AtomikosDataSourceBean;

public class Test123 {

	public static void main(String[] args) throws Exception {

		Properties properties1 = new Properties();
		properties1.put("url", "jdbc:mysql://localhost:3306/test");
		properties1.put("user", "root");
//		properties1.put("password", "root123$");

		UserTransaction transaction = new UserTransactionImp();

		AtomikosDataSourceBean xaDataSourceBean1 = new AtomikosDataSourceBean();
		xaDataSourceBean1.setXaProperties(properties1);
		xaDataSourceBean1.setUniqueResourceName("trans1");
		xaDataSourceBean1.setXaDataSourceClassName("com.mysql.cj.jdbc.MysqlXADataSource");

		transaction.begin();
		Connection con = xaDataSourceBean1.getConnection();
		con.setAutoCommit(false);
		Statement st = con.createStatement();
		st.executeUpdate("insert into tmtest values('1')");
		transaction.commit();

	}

}

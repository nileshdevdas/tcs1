package com.sapient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * @author niles
 * may lead to disasters as there is no threshold monitoring....
 * OutOf Memory. Db Crash , Db LockDown for other user , Non Pooled Connections
 */
public class DatabaseConnectRAW {

	public static void main(String[] args) throws Exception {
		try {
			for (int i = 0; i < 500; i++) {
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("select * from tmtest");
				while (rs.next()) {
					System.out.println(rs.getString(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Thread.sleep(100000);
	}
}

package com.sapient;

import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;

public class KakfaStreamRunWay1Processor {

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, "RWAY1");
		props.put("bootstrap.servers", "192.168.10.100:9092,192.168.10.101:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		StreamsBuilder builder = new StreamsBuilder();
		builder.stream("LANDINGS").filter((name, value) -> {
			System.out.println(new String((byte[]) name));
			return new String((byte[]) name).equals("runway1");
		}).to("RUNWAY1_LANDING");
		KafkaStreams streams = new KafkaStreams(builder.build(), props);
		streams.cleanUp();
		streams.start();
		Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
	}
}

package com.airports.concurrency;

public class Bucket {
     void getWater(){
        try {
            System.out.println("Entry of thread " + Thread.currentThread().getName());
            synchronized (this){
                System.out.println("Who Got the water " + Thread.currentThread().getName());
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Bucket bucket = new Bucket();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                bucket.getWater();
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                bucket.getWater();
            }
        });
        t1.start();
        t2.start();
    }

}

package com.airports.concurrency;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockingDemo {
    private static final Lock mylock  = new ReentrantLock();
    void test(){
         try {
             System.out.println("Entered " + Thread.currentThread().getName());
             mylock.tryLock(5000, TimeUnit.SECONDS);
             System.out.println("Who Got the Water " + Thread.currentThread().getName());
             Thread.sleep(20000);
             mylock.unlock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        LockingDemo demo = new LockingDemo();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                demo.test();
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                demo.test();
            }
        });
        t1.start();
        t2.start();
    }

}

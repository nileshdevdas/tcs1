package com.airports.concurrency;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadLocalDataDemo {
    public static void main(String[] args) {
        ExecutorService service =  Executors.newFixedThreadPool(10);
        for(int i = 0; i<5;i++){
            service.execute(new Runnable() {
                ThreadLocal<Integer> count  = new ThreadLocal<Integer>();
                @Override
                public void run() {
                    count.set(new Random().nextInt());
                    doSomething();
                    displayTheData(count.get(), count.get(), count.get());
                }
                public void doSomething(){
                    System.out.println(Thread.currentThread().getName() + "-" + count.get());
                }

            });
        }
        service.shutdown();
    }
    public static void displayTheData(Integer a, Integer c , Integer d){
        System.out.println(a);
        System.out.println(d);
        System.out.println(c);
        System.out.println(Thread.currentThread().getName());
    }

}

package com.airports.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockDemo {

    public static void main(String[] args) {
        Process process = new Process();
        Thread thread1  = new Thread(new Runnable() {
            @Override
            public void run() {
                process.doProcess();
            }
        });
        Thread thread2  = new Thread(new Runnable() {
            @Override
            public void run() {
                process.doProcess();
            }
        });

        Thread thread3  = new Thread(new Runnable() {
            @Override
            public void run() {
                process.doProcess();
            }
        });
        thread1.start();
        thread2.start();
        thread3.start();
    }

    public static class  Process {
        private final Lock lock = new ReentrantLock();
        public void doProcess(){
            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        }
    }



}

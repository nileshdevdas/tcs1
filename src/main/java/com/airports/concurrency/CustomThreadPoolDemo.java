package com.airports.concurrency;

import java.sql.Array;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CustomThreadPoolDemo {
    public static void main(String[] args) {
        //ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>();
        int MAX_THREADS = 10;
        int MIN_THREADS = 1;
//        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
//        MyRejectionHandler handler = new MyRejectionHandler();
//        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 10 , 10 , TimeUnit.SECONDS,  queue, );
//        Executors.newFixedThreadPool(100);// how many request should be in the queeu
        ThreadPoolExecutor ex = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        ex.setRejectedExecutionHandler(new MyRejectionHandler());
        ex.setCorePoolSize(10);
        System.out.println(ex.getPoolSize());
        System.out.println(ex.getCorePoolSize());
        System.out.println(ex.getMaximumPoolSize());
        System.out.println(ex.getKeepAliveTime(TimeUnit.SECONDS));
        for (int i = 0 ; i < 100 ; i++){
            ex.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(Thread.currentThread().getName());
                        Thread.sleep(100000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        // SLA of processing the OTP is 3 mins but the problem the Thread itself which generated otp is waiting 3 misn
        // now the problem is that otp is already a waste (Reject Job) Time Density Contraining





        // what is the timeout when the queue there how much time the person should wait in the queue
        // when waiting time is over how do i handle the workRejected exception ?
        // Customize all this things
    }
    static class MyRejectionHandler implements  RejectedExecutionHandler {

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            Lock lock = new ReentrantLock();
            System.out.println("Job Rejected........." );
            lock.lock();
            executor.setMaximumPoolSize(executor.getPoolSize() + 10);
            lock.unlock();
                // resubmit the failed job
            executor.submit(r);
            //should i retry ......
        }
    }
}

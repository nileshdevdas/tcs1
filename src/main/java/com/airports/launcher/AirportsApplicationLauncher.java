package com.airports.launcher;

import com.airports.db.AirportsInMemoryDB;
import com.airports.helpers.AirportType;
import com.airports.instrumentation.CacheMBean;
import com.airports.instrumentation.ICacheMBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import java.lang.management.ManagementFactory;


public class AirportsApplicationLauncher {
    private static final Logger logger = LoggerFactory.getLogger(AirportsApplicationLauncher.class);

    public static void main(String[] args) {
        // logger.info("Starting Application Airports ");
        AirportsInMemoryDB.airports();
        if (logger.isInfoEnabled())
            logger.info(AirportType.SMALL_AIRPORT.smallAirport());
        try {
            ObjectName name = new ObjectName("com.airports:type=CacheManager");
            MBeanServer server = ManagementFactory.getPlatformMBeanServer();
            StandardMBean mbean = new StandardMBean(new CacheMBean(), ICacheMBean.class);
            // register this mbean....
            server.registerMBean(mbean, name);
            Thread.sleep(Long.MAX_VALUE);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}


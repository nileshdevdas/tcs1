package com.airports.jvmmetrics;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class VerboseGCDemo {
    List<String> list = new ArrayList<String>();
    /// start with a small footprint for the application ---> 100mb Xms Xmx -->
    public void addData(String somedata){
        list.add(somedata);
    }

    public static void main(String[] args) throws  Exception{
        VerboseGCDemo demo = new VerboseGCDemo();
        TimerTask task = new ReportTask(demo);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 500, 500);
    }
    static class ReportTask extends TimerTask{
        VerboseGCDemo demo ;
        public ReportTask(VerboseGCDemo demo){
            this.demo = demo;
        }
        @Override
        public void run() {
            for(int i = 0; i<100000;i++){
                this.demo.addData("xa09jkldjaioduf9as09dfioausdfjas;ldfj0-fklajsdklfj alsdfjklasdjfklasjdlkfj asldkfjaskldfjklasjdfkl asdklfjaskldfjklasdfjklas dfjasd fkljaskldfjklasdjfk jdsjfklasdjfklasdfl j" + Math.random()+ "");
            }
        }
    }
}

package com.airports.instrumentation;

import com.airports.caching.CacheManager;

public class CacheMBean implements  ICacheMBean{
    private long  lastChangedData;
    @Override
    public int getCacheSize() {
        return CacheManager.getInstance().getCache().size();
    }
    @Override
    public void setCacheSize(int size) {
        System.out.println("Not Yet Implemented....");
    }
    @Override
    public long getLastChangedTime() {
        return lastChangedData;
    }

    @Override
    public void addCacheData(String key, String value) {

        CacheManager.getInstance().addNewCacheData(key,value);
        this.lastChangedData = System.currentTimeMillis();
    }
    @Override
    public void refresh() {
        CacheManager.getInstance().reload();
        this.lastChangedData = System.currentTimeMillis();
    }
    @Override
    public void clear() {
        CacheManager.getInstance().clear();
        this.lastChangedData = System.currentTimeMillis();
    }
}


package com.airports.instrumentation;

public interface ICacheMBean {
    public int getCacheSize();
    public void setCacheSize(int size);
    public long getLastChangedTime();
    public void addCacheData(String key, String value);
    public void refresh();
    public void clear();
}

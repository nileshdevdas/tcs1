package com.airports.caching;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CacheManager {
    private static CacheManager cacheManager = null;

    /**
     * Thread Safe Singleton for Caching....
     * @return
     */
    public static CacheManager getInstance() {
        if (cacheManager != null) {
            return cacheManager;
        } else {
            Lock lock = new ReentrantLock();
            lock.lock();
            cacheManager = new CacheManager();
            lock.unlock();
        }
        return cacheManager;
    }


    private static final Map<String, String> cachedData = new HashMap<String, String>();

    public CacheManager() {
        loadData();
    }

    private void loadData() {
        for (int i = 0; i < 1000; i++) {
            cachedData.put("KEY_" + i, UUID.randomUUID().toString());
        }
    }

    public Map<String, String> getCache() {
        return cachedData;
    }

    public void addNewCacheData(String key, String value) {
        cachedData.put(key, value);
    }

    public void reload() {
        cachedData.clear();
        loadData();
    }

    public void clear() {
        cachedData.clear();
    }
}

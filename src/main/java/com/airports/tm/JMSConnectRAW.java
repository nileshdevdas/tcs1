package com.airports.tm;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class JMSConnectRAW {

	public static void main(String[] args) throws Exception {
		Connection connection = null;
		try {

			connection = new ActiveMQConnectionFactory("tcp://localhost:61616").createConnection("admin", "admin");
			Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue("SAPIENT");
			TextMessage message = session.createTextMessage();
			message.setText("This is a sample Message");
			MessageProducer sender = session.createProducer(queue);
			sender.send(message);
			session.commit();
			session.close();
			connection.close();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

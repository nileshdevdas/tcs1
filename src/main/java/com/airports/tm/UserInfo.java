package com.airports.tm;

import java.io.Serializable;

public class UserInfo implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2079764464486886892L;
	private Long id;
	private String name;
	private String action;
	private String status;

	public String getAction() {
		return action;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return status;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}

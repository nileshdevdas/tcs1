package com.airports.crypto;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.util.Base64;

public class AssymetricCryptoHelper {


    public static String sign(String data) throws Exception {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(getPrivateKey());
        signature.update(data.getBytes());
        return new String(Base64.getEncoder().encode(signature.sign()));
    }

    public static boolean validateSignature(String data, String signedData) throws Exception {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(getPublicKey());
        // actual data
        signature.update(data.getBytes());
        // the signature
        return signature.verify(Base64.getDecoder().decode(signedData.getBytes()));
    }

    /**
     * @param encodedSecret
     * @return String
     * The request will be a encrypted Secret and return will be plain string
     */
    public static String decrypt(String encodedSecret) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, getPrivateKey());
        byte[] decryptedData = cipher.doFinal(Base64.getDecoder().decode(encodedSecret));
        return new String(decryptedData);
    }

    /**
     * @param plainText
     * @return Returns B64Encoded Encrypted String .......
     */
    public static String encrypt(String plainText) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey());
        cipher.update(plainText.getBytes());
        byte[] secretData = cipher.doFinal();
        return new String(Base64.getEncoder().encode(secretData));
    }

    private static PrivateKey getPrivateKey() {
        KeyStore keyStore = null;
        PrivateKey key = null;
        try {
            keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream("d:/vinsys.jks"), "root123".toCharArray());
            KeyStore.ProtectionParameter parameter = new KeyStore.PasswordProtection("root123".toCharArray());
            key = (PrivateKey) keyStore.getKey("vinsys", "root123".toCharArray());
        } catch (Exception e) {
            // you can handle your exceptions here
        } finally {
        }
        return key;
    }

    private static PublicKey getPublicKey() {
        KeyStore keyStore = null;
        PublicKey key = null;
        try {
            keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream("d:/vinsys.jks"), "root123".toCharArray());
            key = keyStore.getCertificate("vinsys").getPublicKey();
        } catch (Exception e) {
            // you can handle your exceptions here
        } finally {
        }
        return key;
    }

    public static void main(String[] args) throws Exception {
        //String encryptedString = AssymetricCryptoHelper.encrypt("This is superb");
        // System.out.println(encryptedString);
        //String plainText = AssymetricCryptoHelper.decrypt(encryptedString);
        String signedData = AssymetricCryptoHelper.sign("Nilesh please handover 3000 rs to jayesh");
        System.out.println(signedData);
        boolean isValid= AssymetricCryptoHelper.validateSignature("Nilesh please handover 3000 rs to jayesh", signedData);
        System.out.println(isValid);
    }
}

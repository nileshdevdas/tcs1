package com.airports.crypto.examples;

import javax.crypto.Cipher;
import java.security.*;

public class AssymetricEncryption {

    public static void main(String[] args) throws Exception{

        // can programmaticlly generate a key pair ---> Yes
        // can i read a already generated keypari programmaticlly -> yes
        // Can i read a public key programaticlly -> Yes
        // can i read a private key Programaticlly -> Yes (Provided you have the password)
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        /// Encryption is done with public key
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        /// private key
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
    }
}

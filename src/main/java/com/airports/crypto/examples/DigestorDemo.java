package com.airports.crypto.examples;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class DigestorDemo {

    public static void main(String[] args) throws Exception{
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update("password".getBytes());
        byte [] myhash = digest.digest();
        StringBuffer hexString = new StringBuffer();
        for(int i = 0 ; i<myhash.length; i++){
            hexString.append(Integer.toHexString(0xFF &  myhash[i]));
        }
        System.out.println("Your Encrypted String " + hexString.toString());
    }
}

package com.airports.crypto.examples;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigestSpi;
import java.security.SecureRandom;
import java.util.Base64;

public class SymmetricEncryption {
    public static void main(String[] args) throws Exception{
        /**
         * Hashing + Salt =   A unique hash Code ---> Which Never change if the Input String and The Salt remains
         *  i will only compare outomes of 2 strings or 2 data if both the outcomes are same
         *  --> One way and you never want to decrypt reverse them (Irrevesible data  Encoders) ---> expected.hashcode == actual.hashcode
         *  Database Back ---> Password ---> Backup file disk ---> disk is lost this is you banking password --->
         *
         * Symmetric  You Encrypt using using the Same Key and You Decrypt using the Same Key
         * U have a database and you dont want the password to be visible to pepople as you are putting this in the code scm or
         * properties file or some other odd things ---. So you would want to keep this secured
         * Symmetric Key to be used ---> to encrypt and decrypt  99.9999999999999999999
         *
         *  I need the Secret Key and using the Secret Key i will do the need ful on both the side
         * PKI Asymmetric  You Encrypt using KeyPairs ---> Encryption happens using public Key and Decryption happens using private key
         * (Jks File ) / KeyPair and Using KeyPair i will do the Ciphering
         *
         * Between two different parties where the sender is not a internal team or connect or from the controlled environment
         * bank ever give you symmetric key ? where you encrypt as well as decrypt ---> Uncontrolled Environments
         * Where you dont want to give control to the encrypting party the right of decryption and u want to seperate those rights
         *
         *
         *
         * Encoding --->
         *      Message
         *      LHS = RHS
         *      LHS = Intefaces Or contract  (MessageDigest).............
         *      RHS = Service provider Implementation (SPI)
         *      api == contract
         *      spi == impl  (By implement SPI Interfaces)
         */
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(new SecureRandom());
        SecretKey mysecretKey = keyGenerator.generateKey();
        System.out.println(mysecretKey.getAlgorithm());
        System.out.println(mysecretKey.getFormat());
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, mysecretKey);
        byte data [] = cipher.doFinal("HelloWorld".getBytes());
        System.out.println(new String(Base64.getEncoder().encode(data)));

        cipher.init(Cipher.DECRYPT_MODE, mysecretKey);

    }

}
